package com.ls.lps._config;




import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class LeisureManagementRestTemplateConfig {

    @Value("${app.leisure.management.api.username}")
    private String apiUsername;
    @Value("${app.leisure.management.api.pass}")
    private String apiPass;

    @Bean
    public RestTemplate leisureManagementRestTemplate(RestTemplateBuilder builder) {
        return builder
                .basicAuthentication(apiUsername, apiPass)
                .build();
    }
}
