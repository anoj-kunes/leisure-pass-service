package com.ls.lps.controller;

import com.ls.lps.dto.TouristPassDto;
import com.ls.lps.dto.api.CustomerDto;
import com.ls.lps.dto.input.TouristPassInputDto;
import com.ls.lps.exception.DataNotFoundException;
import com.ls.lps.exception.DataValidationException;
import com.ls.lps.service.TouristPassService;
import com.ls.lps.service.api.CustomerService;
import com.ls.lps.validation.CancelGroup;
import com.ls.lps.validation.CreateGroup;
import com.ls.lps.validation.RenewGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("${app.api.url}/tourist-passes")
public class TouristPassController {

    @Autowired
    private TouristPassService touristPassService;

    @GetMapping(value = "/{id}/active")
    public ResponseEntity<Map<String, Boolean>> touristPassActive(@PathVariable String id) throws DataNotFoundException {
        return new ResponseEntity<>(touristPassService.touristPassActive(id), HttpStatus.OK);
    }

    @PostMapping(value = "/add")
    public ResponseEntity<TouristPassDto> addPass(@RequestBody @Validated({CreateGroup.class}) TouristPassInputDto inputDto, BindingResult result) throws DataValidationException {
        if (result.hasErrors()) {
            throw new DataValidationException(result);
        }
        return new ResponseEntity<>(touristPassService.createTouristPass(inputDto), HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}/cancel")
    public ResponseEntity<TouristPassDto> cancelPass(@PathVariable String id) throws DataNotFoundException {
        return new ResponseEntity<>(touristPassService.cancelTouristPass(id), HttpStatus.OK);
    }

    @PutMapping(value = "/renew")
    public ResponseEntity<TouristPassDto> renewPass(@RequestBody @Validated({RenewGroup.class}) TouristPassInputDto inputDto, BindingResult result) throws DataValidationException {
        if (result.hasErrors()) {
            throw new DataValidationException(result);
        }
        return new ResponseEntity<>(touristPassService.renewTouristPass(inputDto), HttpStatus.OK);
    }
}
