package com.ls.lps.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.ls.lps.dto.input.TouristPassInputDto;
import com.ls.lps.util.JsonDateTimeSerializer;
import com.ls.lps.validation.CreateGroup;
import com.ls.lps.validation.RenewGroup;
import lombok.Data;
import org.joda.time.DateTime;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class TouristPassDto extends TouristPassInputDto {
    private String city;
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    private DateTime issuedAt;
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    private DateTime expiredAt;

    @Override
    @JsonIgnore
    public @Min(value = 1, groups = {CreateGroup.class, RenewGroup.class}) @Max(value = 730, groups = {CreateGroup.class, RenewGroup.class}) @NotNull(groups = {CreateGroup.class, RenewGroup.class}) Long getPassLengthInHours() {
        return super.getPassLengthInHours();
    }
}
