package com.ls.lps.dto.api;

import lombok.Data;

@Data
public class CustomerDto {
    private String id;
    private String email;
    private String firstName;
    private String lastName;
    private String city;
}
