package com.ls.lps.dto.api;

import lombok.Data;

@Data
public class VendorDto {
    private String id;
    private String vendor;
    private String city;
}
