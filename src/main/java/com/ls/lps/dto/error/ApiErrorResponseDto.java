package com.ls.lps.dto.error;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ApiErrorResponseDto extends ErrorResponseDto {
    private HttpStatus status;
}
