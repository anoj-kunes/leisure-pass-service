package com.ls.lps.dto.error;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ErrorResponseDetailsDto {
    private String error;

    public ErrorResponseDetailsDto(String error) {
        this.error = error;
    }
}
