package com.ls.lps.dto.error;

import lombok.Data;

import java.util.List;

@Data
public class ErrorResponseDto {
    private String type;
    private List<ErrorResponseDetailsDto> errors;

    public ErrorResponseDto(String type, List<ErrorResponseDetailsDto> errors) {
        this.errors = errors;
        this.type = type;
    }

    public ErrorResponseDto() {
    }
}
