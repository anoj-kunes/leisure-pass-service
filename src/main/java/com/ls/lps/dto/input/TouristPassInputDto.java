package com.ls.lps.dto.input;

import com.ls.lps.validation.CancelGroup;
import com.ls.lps.validation.CreateGroup;
import com.ls.lps.validation.RenewGroup;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class TouristPassInputDto {
    @NotEmpty(groups = { RenewGroup.class })
    @NotNull(groups = { RenewGroup.class })
    protected String touristPassId;
    @NotEmpty(groups = { CreateGroup.class })
    @NotNull(groups = { CreateGroup.class })
    protected String customerId;
    @NotEmpty(groups = { CreateGroup.class })
    @NotNull(groups = { CreateGroup.class })
    protected String vendorId;
    @Min(value = 1,  groups = { CreateGroup.class, RenewGroup.class })
    @Max(value = 730,  groups = { CreateGroup.class, RenewGroup.class })
    @NotNull(groups = { CreateGroup.class, RenewGroup.class  })
    protected Long passLengthInHours;
}
