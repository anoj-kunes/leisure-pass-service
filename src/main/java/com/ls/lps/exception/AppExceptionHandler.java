package com.ls.lps.exception;

import com.ls.lps.dto.error.ApiErrorResponseDto;
import com.ls.lps.dto.error.ErrorResponseDetailsDto;
import com.ls.lps.dto.error.ErrorResponseDto;
import com.ls.lps.exception.api.ApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {
    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(ApiException.class)
    public ResponseEntity<Object> unhandledApiException(ApiException e, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ApiErrorResponseDto responseDto = e.getErrorResponse();
        ErrorResponseDto error = new ErrorResponseDto(responseDto.getType(), responseDto.getErrors());
        return handleExceptionInternal(e, error, headers, responseDto.getStatus(), request);
    }

    @ExceptionHandler({DataNotFoundException.class})
    public ResponseEntity<Object> handleDataNotFoundException(DataNotFoundException e, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ErrorResponseDto error = new ErrorResponseDto("DATA_NOT_FOUND", Collections.singletonList(new ErrorResponseDetailsDto(e.getMessage())));
        return handleExceptionInternal(e, error, headers, HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler({DataValidationException.class})
    public ResponseEntity<Object> handleDataValidationException(DataValidationException e, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ErrorResponseDto error = new ErrorResponseDto("DATA_VALIDATION_ERROR", Collections.singletonList(this.getValidationErrors(e.getBindingResult())));
        return handleExceptionInternal(e, error, headers, HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> unhandledException(Exception e, WebRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        ErrorResponseDto error = new ErrorResponseDto("INTERNAL_SERVER_ERROR", Collections.singletonList(new ErrorResponseDetailsDto(e.getMessage())));
        return handleExceptionInternal(e, error, headers, HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    private ErrorResponseDetailsDto getValidationErrors(BindingResult result) {
        List<FieldError> fieldErrors = result.getFieldErrors();
        List<String> errors = new ArrayList<>();
        for (FieldError fieldError : fieldErrors) {
            String fieldName = fieldError.getField().replaceAll("\\[(.*?)\\]", "");
            String message = fieldError.getDefaultMessage();
            try {
                message = messageSource.getMessage(
                        (fieldError.getObjectName() + "." + fieldName + "." + fieldError.getCode()).replaceAll("\\[(.*?)\\]", ""),
                        fieldError.getRejectedValue() != null ? new String[]{fieldError.getRejectedValue().toString()} : null,
                        null
                );
            } catch (Exception e) {
            }

            errors.add(message);
        }

        return new ErrorResponseDetailsDto(String.join(", ", errors));
    }
}
