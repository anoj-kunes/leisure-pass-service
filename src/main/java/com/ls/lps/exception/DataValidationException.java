package com.ls.lps.exception;

import lombok.Data;
import org.springframework.validation.BindingResult;

@Data
public class DataValidationException extends Exception {
    private BindingResult bindingResult;

    public DataValidationException(BindingResult bindingResult) {
        super("Data validations error");
        this.bindingResult = bindingResult;
    }
}
