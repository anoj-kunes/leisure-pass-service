package com.ls.lps.exception.api;

import com.ls.lps.dto.error.ApiErrorResponseDto;
import com.ls.lps.dto.error.ErrorResponseDto;
import lombok.Data;

@Data
public class ApiException extends RuntimeException {
    private ApiErrorResponseDto errorResponse;

    public ApiException(ApiErrorResponseDto errorResponse) {
        super("API Error");
        this.errorResponse = errorResponse;
    }
}
