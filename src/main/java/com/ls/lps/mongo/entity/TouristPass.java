package com.ls.lps.mongo.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@Document
public class TouristPass {
    @Id
    private String id;
    private String customerId;
    private String vendorId;
    private String city;
    private Date expiredAt;
    private Date issuedAt;
}
