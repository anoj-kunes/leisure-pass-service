package com.ls.lps.mongo.repository;

import com.ls.lps.mongo.entity.TouristPass;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TouristPassRepository extends MongoRepository<TouristPass, String> {
}
