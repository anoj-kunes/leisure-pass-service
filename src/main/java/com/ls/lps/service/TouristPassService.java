package com.ls.lps.service;

import com.ls.lps.dto.TouristPassDto;
import com.ls.lps.dto.api.CustomerDto;
import com.ls.lps.dto.api.VendorDto;
import com.ls.lps.dto.input.TouristPassInputDto;
import com.ls.lps.exception.DataNotFoundException;
import com.ls.lps.mongo.entity.TouristPass;
import com.ls.lps.mongo.repository.TouristPassRepository;
import com.ls.lps.service.api.CustomerService;
import com.ls.lps.service.api.VendorService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.Map;

@Service
public class TouristPassService {
    @Autowired
    private TouristPassRepository touristPassRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private VendorService vendorService;

    public TouristPassDto createTouristPass(TouristPassInputDto inputDto) {
        this.validate(inputDto);

        VendorDto vendorDto = vendorService.findById(inputDto.getVendorId());
        CustomerDto customerDto = customerService.findById(inputDto.getCustomerId());

        DateTime issuedAt = new DateTime();

        TouristPass touristPass = TouristPass.builder()
                .issuedAt(issuedAt.toDate())
                .city(vendorDto.getCity())
                .customerId(inputDto.getCustomerId())
                .vendorId(inputDto.getVendorId())
                .expiredAt(issuedAt.plusHours(inputDto.getPassLengthInHours().intValue()).toDate())
                .build();

        TouristPass savedTouristPass = touristPassRepository.save(touristPass);
        return this.convertEntityToDto(savedTouristPass);
    }

    public TouristPassDto cancelTouristPass(String id) {
        TouristPass touristPass = this.getTouristPass(id);

        touristPassRepository.delete(touristPass);

        return convertEntityToDto(touristPass);
    }

    public TouristPassDto renewTouristPass(TouristPassInputDto inputDto) {
        this.validate(inputDto);
        TouristPass touristPass = this.getTouristPass(inputDto.getTouristPassId());

        touristPass.setExpiredAt(
                new DateTime(touristPass.getExpiredAt())
                        .plusHours(inputDto.getPassLengthInHours().intValue())
                        .toDate()
        );

        TouristPass updatedTouristPass = touristPassRepository.save(touristPass);
        return convertEntityToDto(updatedTouristPass);
    }

    public Map<String, Boolean> touristPassActive(String id) {
        TouristPass touristPass = getTouristPass(id);

        return Collections.singletonMap("status", new DateTime(touristPass.getExpiredAt()).isAfterNow());
    }

    private TouristPass getTouristPass(String id) {
        return touristPassRepository.findById(id)
                .orElseThrow(() -> new DataNotFoundException("Tourist Pass Id not found for #" + id));
    }

    private void validate(TouristPassInputDto inputDto) {
        Assert.notNull(inputDto.getPassLengthInHours(), "Pass length in hours cannot be null");
        Assert.isTrue(inputDto.getPassLengthInHours().intValue() > 0, "Pass in length(in Hours) cannot be less than 1 Hour");
        Assert.isTrue(inputDto.getPassLengthInHours().intValue() <= 730, "Pass in length(in Hours) cannot be less than 730 Hour");
    }

    public TouristPassDto convertEntityToDto(TouristPass touristPass) {
        TouristPassDto touristPassDto = new TouristPassDto();
        touristPassDto.setVendorId(touristPass.getVendorId());
        touristPassDto.setCity(touristPass.getCity());
        touristPassDto.setIssuedAt(new DateTime(touristPass.getIssuedAt()));
        touristPassDto.setCustomerId(touristPass.getCustomerId());
        touristPassDto.setExpiredAt(new DateTime(touristPass.getExpiredAt()));
        touristPassDto.setTouristPassId(touristPass.getId());
        return touristPassDto;
    }
}
