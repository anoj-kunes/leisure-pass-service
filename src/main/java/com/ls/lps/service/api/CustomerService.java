package com.ls.lps.service.api;

import com.ls.lps.dto.api.CustomerDto;
import com.ls.lps.exception.api.ApiErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class CustomerService {
    @Autowired
    private RestTemplate leisureManagementRestTemplate;

    @Value("${app.leisure.management.api.url}")
    private String customerUrl;

    public CustomerDto findById(String id) {
        String url = customerUrl + "/customers/" + id;

        leisureManagementRestTemplate.setErrorHandler(new ApiErrorHandler());

        ResponseEntity<CustomerDto> response = leisureManagementRestTemplate.exchange(url, HttpMethod.GET, new HttpEntity("", new HttpHeaders()), new
                ParameterizedTypeReference<CustomerDto>() {});

        return response.getBody();
    }
}
