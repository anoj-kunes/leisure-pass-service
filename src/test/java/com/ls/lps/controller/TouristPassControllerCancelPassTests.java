package com.ls.lps.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.ls.lps.dto.TouristPassDto;
import com.ls.lps.dto.error.ApiErrorResponseDto;
import com.ls.lps.dto.error.ErrorResponseDetailsDto;
import com.ls.lps.dto.input.TouristPassInputDto;
import com.ls.lps.exception.AppExceptionHandler;
import com.ls.lps.exception.DataNotFoundException;
import com.ls.lps.exception.api.ApiException;
import com.ls.lps.service.TouristPassService;
import org.joda.time.DateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.print.attribute.standard.Media;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = "spring.profiles.active=unit-test")
@ExtendWith(SpringExtension.class)
@EnableWebMvc
public class TouristPassControllerCancelPassTests {
    private MockMvc mockMvc;

    @MockBean
    private TouristPassService touristPassService;

    @Autowired
    private TouristPassController touristPassController;

    @Autowired
    private AppExceptionHandler appExceptionHandler;

    private String uri = "/api/v1/tourist-passes";

    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(touristPassController)
                .setMessageConverters(new MappingJackson2HttpMessageConverter())
                .setControllerAdvice(appExceptionHandler)
                .addPlaceholderValue("app.api.url", "/api/v1")
                .build();
    }

    @Test
    public void cancelPass_validId_methodCallsAndReturnTouristPassInfo() throws Exception {
        String id = "5e246ab08362930f9a15ec2l";

        Mockito.lenient().doAnswer((invocationOnMock) -> {
            TouristPassDto touristPassDto = new TouristPassDto();
            touristPassDto.setVendorId("5e246ab08362930f9a15ec4l");
            touristPassDto.setCustomerId("5e246ab08362930f9a15ec5l");
            touristPassDto.setTouristPassId("5e246ab08362930f9a15ec2l");
            touristPassDto.setExpiredAt(new DateTime().plusHours(5));
            touristPassDto.setIssuedAt(new DateTime());
            touristPassDto.setCity("London");
            return touristPassDto;
        }).when(touristPassService).cancelTouristPass(id);

        mockMvc.perform(MockMvcRequestBuilders.delete(uri + "/" + id + "/cancel")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.touristPassId", is("5e246ab08362930f9a15ec2l")));
    }
}
