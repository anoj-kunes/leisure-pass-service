package com.ls.lps.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.ls.lps.dto.TouristPassDto;
import com.ls.lps.dto.error.ApiErrorResponseDto;
import com.ls.lps.dto.error.ErrorResponseDetailsDto;
import com.ls.lps.dto.input.TouristPassInputDto;
import com.ls.lps.exception.AppExceptionHandler;
import com.ls.lps.exception.api.ApiException;
import com.ls.lps.service.TouristPassService;
import org.joda.time.DateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.is;

import javax.swing.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = "spring.profiles.active=unit-test")
@ExtendWith(SpringExtension.class)
@EnableWebMvc
public class TouristPassControllerCreateTouristPassTests {
    private MockMvc mockMvc;

    @MockBean
    private TouristPassService touristPassService;

    @Autowired
    private TouristPassController touristPassController;

    @Autowired
    private AppExceptionHandler appExceptionHandler;

    private String uri = "/api/v1/tourist-passes";

    private ObjectMapper objectMapper = new ObjectMapper();

    private final Map<String, Object> value = new HashMap<String, Object>();

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(touristPassController)
                .setMessageConverters(new MappingJackson2HttpMessageConverter())
                .setControllerAdvice(appExceptionHandler)
                .addPlaceholderValue("app.api.url", "/api/v1")
                .build();

        value.put("customerId", "5e24676fae7d22144c65bab9");
        value.put("vendorId", "5e246ab08362930f9a15ec2c");
        value.put("passLengthInHours", 5);
    }

    @Test
    public void createTouristPass_validInput_methodCallsAndReturnTouristPassInfo() throws Exception {
        String json = objectMapper.writeValueAsString(value);
        TouristPassInputDto inputDto = objectMapper.readValue(json, TouristPassInputDto.class);

        Mockito.lenient().doAnswer((invocationOnMock) -> {
            TouristPassDto touristPassDto = new TouristPassDto();
            touristPassDto.setVendorId(inputDto.getVendorId());
            touristPassDto.setCustomerId(inputDto.getCustomerId());
            touristPassDto.setTouristPassId("5e246ab08362930f9a15ec2l");
            touristPassDto.setExpiredAt(new DateTime().plusHours(inputDto.getPassLengthInHours().intValue()));
            touristPassDto.setIssuedAt(new DateTime());
            touristPassDto.setCity("London");
            return touristPassDto;
        }).when(touristPassService).createTouristPass(inputDto);

        mockMvc.perform(MockMvcRequestBuilders.post(uri + "/add")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.touristPassId", is("5e246ab08362930f9a15ec2l")));
    }

    @Test
    public void createTouristPass_nonExistingCustomerId_throwsApiException() throws Exception {
        String json = objectMapper.writeValueAsString(value);
        TouristPassInputDto inputDto = objectMapper.readValue(json, TouristPassInputDto.class);

        ApiErrorResponseDto errorResponseDto = new ApiErrorResponseDto();
        errorResponseDto.setStatus(HttpStatus.NOT_FOUND);

        errorResponseDto.setType("DATA_NOT_AVAILABLE");
        errorResponseDto.setErrors(Arrays.asList(new ErrorResponseDetailsDto("Customer not found for #" + inputDto.getCustomerId())));

        Mockito.lenient().doAnswer((invocationOnMock) -> {
            throw new ApiException(errorResponseDto);
        }).when(touristPassService).createTouristPass(inputDto);

        mockMvc.perform(MockMvcRequestBuilders.post(uri + "/add")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.type", is("DATA_NOT_AVAILABLE")))
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[0].error", containsString("Customer not found for #" + inputDto.getCustomerId())));
    }

    @Test
    public void createTouristPass_nonExistingVendorId_throwsApiException() throws Exception {
        String json = objectMapper.writeValueAsString(value);
        TouristPassInputDto inputDto = objectMapper.readValue(json, TouristPassInputDto.class);

        ApiErrorResponseDto errorResponseDto = new ApiErrorResponseDto();
        errorResponseDto.setStatus(HttpStatus.NOT_FOUND);

        errorResponseDto.setType("DATA_NOT_AVAILABLE");
        errorResponseDto.setErrors(Arrays.asList(new ErrorResponseDetailsDto("Vendor not found for #" + inputDto.getVendorId())));

        Mockito.lenient().doAnswer((invocationOnMock) -> {
            throw new ApiException(errorResponseDto);
        }).when(touristPassService).createTouristPass(inputDto);

        mockMvc.perform(MockMvcRequestBuilders.post(uri + "/add")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.type", is("DATA_NOT_AVAILABLE")))
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[0].error", containsString("Vendor not found for #" + inputDto.getVendorId())));
    }

    @Test
    public void createTouristPass_invalidInput_throwsDataValidationException() throws Exception {
        value.put("customerId", null);
        String json = objectMapper.writeValueAsString(value);
        TouristPassInputDto inputDto = objectMapper.readValue(json, TouristPassInputDto.class);

        mockMvc.perform(MockMvcRequestBuilders.post(uri + "/add")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.type", is("DATA_VALIDATION_ERROR")))
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[0].error", containsString("Customer Id cannot be null")));
    }
}
