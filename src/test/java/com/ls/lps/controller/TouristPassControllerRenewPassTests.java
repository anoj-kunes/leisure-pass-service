package com.ls.lps.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.ls.lps.dto.TouristPassDto;
import com.ls.lps.dto.error.ApiErrorResponseDto;
import com.ls.lps.dto.error.ErrorResponseDetailsDto;
import com.ls.lps.dto.input.TouristPassInputDto;
import com.ls.lps.exception.AppExceptionHandler;
import com.ls.lps.exception.DataNotFoundException;
import com.ls.lps.exception.api.ApiException;
import com.ls.lps.service.TouristPassService;
import org.joda.time.DateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = "spring.profiles.active=unit-test")
@ExtendWith(SpringExtension.class)
@EnableWebMvc
public class TouristPassControllerRenewPassTests {
    private MockMvc mockMvc;

    @MockBean
    private TouristPassService touristPassService;

    @Autowired
    private TouristPassController touristPassController;

    @Autowired
    private AppExceptionHandler appExceptionHandler;

    private String uri = "/api/v1/tourist-passes";

    private ObjectMapper objectMapper = new ObjectMapper();

    private final Map<String, Object> value = new HashMap<String, Object>();

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(touristPassController)
                .setMessageConverters(new MappingJackson2HttpMessageConverter())
                .setControllerAdvice(appExceptionHandler)
                .addPlaceholderValue("app.api.url", "/api/v1")
                .build();

        value.put("touristPassId", "5e24676fae7d22144c65bab9");
        value.put("passLengthInHours", 5);
    }

    @Test
    public void renewPass_validInput_methodCallsAndReturnTouristPassInfo() throws Exception {
        String json = objectMapper.writeValueAsString(value);
        TouristPassInputDto inputDto = objectMapper.readValue(json, TouristPassInputDto.class);

        Mockito.lenient().doAnswer((invocationOnMock) -> {
            TouristPassDto touristPassDto = new TouristPassDto();
            touristPassDto.setVendorId(inputDto.getVendorId());
            touristPassDto.setCustomerId(inputDto.getCustomerId());
            touristPassDto.setTouristPassId("5e246ab08362930f9a15ec2l");
            touristPassDto.setExpiredAt(new DateTime().plusHours(inputDto.getPassLengthInHours().intValue()));
            touristPassDto.setIssuedAt(new DateTime());
            touristPassDto.setCity("London");
            return touristPassDto;
        }).when(touristPassService).renewTouristPass(inputDto);

        mockMvc.perform(MockMvcRequestBuilders.put(uri + "/renew")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.touristPassId", is("5e246ab08362930f9a15ec2l")));
    }

    @Test
    public void renewPass_nonExistingPassId_throwsDataNotFoundException() throws Exception {
        String json = objectMapper.writeValueAsString(value);
        TouristPassInputDto inputDto = objectMapper.readValue(json, TouristPassInputDto.class);

        Mockito.lenient().doAnswer((invocationOnMock) -> {
            throw new DataNotFoundException("Tourist Pass not found for #" + inputDto.getTouristPassId());
        }).when(touristPassService).renewTouristPass(inputDto);

        mockMvc.perform(MockMvcRequestBuilders.put(uri + "/renew")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.type", is("DATA_NOT_FOUND")))
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[0].error", containsString("Tourist Pass not found for #" + inputDto.getTouristPassId())));
    }

    @Test
    public void renewPass_passLengthIsLessThanMinimum_throwsDataValidationException() throws Exception {
        value.put("passLengthInHours", 0);
        String json = objectMapper.writeValueAsString(value);
//        TouristPassInputDto inputDto = objectMapper.readValue(json, TouristPassInputDto.class);


        mockMvc.perform(MockMvcRequestBuilders.put(uri + "/renew")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(json))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.type", is("DATA_VALIDATION_ERROR")))
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[0].error", containsString("Pass in length(in Hours) cannot be less than 1 Hour")));
    }
}
