package com.ls.lps.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.ls.lps.dto.TouristPassDto;
import com.ls.lps.exception.AppExceptionHandler;
import com.ls.lps.exception.DataNotFoundException;
import com.ls.lps.service.TouristPassService;
import org.joda.time.DateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = "spring.profiles.active=unit-test")
@ExtendWith(SpringExtension.class)
@EnableWebMvc
public class TouristPassControllerTouristPassActiveTests {
    private MockMvc mockMvc;

    @MockBean
    private TouristPassService touristPassService;

    @Autowired
    private TouristPassController touristPassController;

    @Autowired
    private AppExceptionHandler appExceptionHandler;

    private String uri = "/api/v1/tourist-passes";

    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(touristPassController)
                .setMessageConverters(new MappingJackson2HttpMessageConverter())
                .setControllerAdvice(appExceptionHandler)
                .addPlaceholderValue("app.api.url", "/api/v1")
                .build();
    }

    @Test
    public void touristPassActive_validId_methodCallsAndReturnStatus() throws Exception {
        String id = "5e246ab08362930f9a15ec2l";

        Mockito.lenient().doAnswer((invocationOnMock) -> {
            return Collections.singletonMap("status", false);
        }).when(touristPassService).touristPassActive(id);

        mockMvc.perform(MockMvcRequestBuilders.get(uri + "/" + id + "/active")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(false)));
    }

    @Test
    public void touristPassActive_nonValidId_throwsDataNotFoundException() throws Exception {
        String id = "5e246ab08362930f9a15ec2l";

        Mockito.lenient().doAnswer((invocationOnMock) -> {
            throw new DataNotFoundException("Tourist pass not found for #" + id);
        }).when(touristPassService).touristPassActive(id);

        mockMvc.perform(MockMvcRequestBuilders.get(uri + "/" + id + "/active")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.type", is("DATA_NOT_FOUND")))
                .andExpect(jsonPath("$.errors", hasSize(1)))
                .andExpect(jsonPath("$.errors[0].error", containsString("Tourist pass not found for #" + id)));
    }
}
