package com.ls.lps.service;

import com.ls.lps.dto.TouristPassDto;
import com.ls.lps.dto.api.CustomerDto;
import com.ls.lps.dto.api.VendorDto;
import com.ls.lps.dto.error.ApiErrorResponseDto;
import com.ls.lps.dto.input.TouristPassInputDto;
import com.ls.lps.exception.DataNotFoundException;
import com.ls.lps.exception.api.ApiException;
import com.ls.lps.mongo.entity.TouristPass;
import com.ls.lps.mongo.repository.TouristPassRepository;
import com.ls.lps.service.api.CustomerService;
import com.ls.lps.service.api.VendorService;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@Slf4j
@ExtendWith(MockitoExtension.class)
@SpringBootTest(properties = "spring.profiles.active=unit-test")
public class TouristPassServiceCancelTouristPassTests {

    @Mock
    private CustomerService customerService;

    @Mock
    private VendorService vendorService;

    @Mock
    private TouristPassRepository touristPassRepository;

    @InjectMocks
    private TouristPassService touristPassService;

    @Test
    public void cancelTouristPass_validTouristIdIsGiven_methodCancelsAndReturnsTouristPassInfo() {
        DateTime dateTime = new DateTime();
        TouristPass touristPass = getTouristData(dateTime);
        Mockito.lenient().when(touristPassRepository.findById("5e246ab08362930f9a15ec2d"))
                .thenReturn(Optional.of(touristPass));

        Mockito.lenient().doNothing().when(touristPassRepository).delete(any(TouristPass.class));

        TouristPassDto touristPassDto = touristPassService.cancelTouristPass("5e246ab08362930f9a15ec2d");
        Assertions.assertEquals(touristPassDto, touristPassService.convertEntityToDto(touristPass));
        Mockito.verify(touristPassRepository, Mockito.times(1)).findById("5e246ab08362930f9a15ec2d");
        Mockito.verify(touristPassRepository, Mockito.times(1)).delete(any(TouristPass.class));
    }

    @Test
    public void cancelTouristPass_invalidTouristIdIsGiven_throwsDataNotFoundException() {
        Mockito.lenient().when(touristPassRepository.findById(null))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(DataNotFoundException.class, () -> { touristPassService.cancelTouristPass(null);; }, "Tourist not found for #null");

        Mockito.verify(touristPassRepository, Mockito.times(1)).findById(null);
        Mockito.verify(touristPassRepository, Mockito.times(0)).delete(any(TouristPass.class));
    }



    private TouristPass getTouristData(DateTime dateTime) {
        Date expired = dateTime.plusHours(5).toDate();
        return TouristPass.builder()
                .id("5e246ab08362930f9a15ec2d")
                .expiredAt(expired)
                .city("London")
                .issuedAt(dateTime.toDate())
                .customerId("5e24676fae7d22144c65babb")
                .vendorId("5e246ab08362930f9a15ec2c").build();
    }

}
