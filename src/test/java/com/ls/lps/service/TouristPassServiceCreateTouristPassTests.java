package com.ls.lps.service;

import com.ls.lps.dto.TouristPassDto;
import com.ls.lps.dto.api.CustomerDto;
import com.ls.lps.dto.api.VendorDto;
import com.ls.lps.dto.error.ApiErrorResponseDto;
import com.ls.lps.dto.input.TouristPassInputDto;
import com.ls.lps.exception.api.ApiException;
import com.ls.lps.mongo.entity.TouristPass;
import com.ls.lps.mongo.repository.TouristPassRepository;
import com.ls.lps.service.api.CustomerService;
import com.ls.lps.service.api.VendorService;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Date;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@Slf4j
@ExtendWith(MockitoExtension.class)
@SpringBootTest(properties = "spring.profiles.active=unit-test")
public class TouristPassServiceCreateTouristPassTests {

    @Mock
    private CustomerService customerService;

    @Mock
    private VendorService vendorService;

    @Mock
    private TouristPassRepository touristPassRepository;

    @InjectMocks
    private TouristPassService touristPassService;

    private final TouristPassInputDto touristPassInputDto = new TouristPassInputDto();
    private final CustomerDto customerDto = new CustomerDto();
    private final VendorDto vendorDto = new VendorDto();

    @BeforeEach
    public void setUp() {
        touristPassInputDto.setCustomerId("5e24676fae7d22144c65babb");
        touristPassInputDto.setPassLengthInHours(5L);
        touristPassInputDto.setVendorId("5e246ab08362930f9a15ec2c");

        customerDto.setId("5e24676fae7d22144c65babb");
        customerDto.setCity("London");
        customerDto.setFirstName("Anoj");
        customerDto.setFirstName("Kunes");
        customerDto.setEmail("anojkunes@googlemail.com");

        vendorDto.setCity("London");
        vendorDto.setId("5e246ab08362930f9a15ec2c");
        vendorDto.setVendor("lon-vendor");
    }

    @Test
    public void createTouristPass_touristPassInputIsFilled_methodCreatesAndReturnsTouristPassInfo() {
        log.info("method calls");
        Mockito.lenient().when(customerService.findById(touristPassInputDto.getCustomerId()))
                .thenReturn(customerDto);
        Mockito.lenient().when(vendorService.findById(touristPassInputDto.getVendorId())).thenReturn(vendorDto);

        DateTime dateTime = new DateTime();
        TouristPass touristPass = getTouristData(dateTime);
        TouristPass savedTouristPass = getTouristData(dateTime);
        savedTouristPass.setId("5e246ab08362930f9a15ec2d");

        TouristPassDto expected = touristPassService.convertEntityToDto(savedTouristPass);

        Mockito.lenient().when(touristPassRepository.save(any())).thenReturn(touristPass);

        TouristPassDto touristPassDto = touristPassService.createTouristPass(touristPassInputDto);
        Assertions.assertEquals(touristPassDto, expected);
        Mockito.verify(customerService, Mockito.times(1)).findById(touristPassInputDto.getCustomerId());
        Mockito.verify(vendorService, Mockito.times(1)).findById(touristPassInputDto.getVendorId());
        Mockito.verify(touristPassRepository, Mockito.times(1)).save(any(TouristPass.class));
    }

    @Test
    public void createTouristPass_vendorIdIsNull_throwsApiException() {
        touristPassInputDto.setVendorId(null);
        Mockito.lenient().when(customerService.findById(touristPassInputDto.getCustomerId()))
                .thenReturn(customerDto);
        Mockito.lenient().when(vendorService.findById(touristPassInputDto.getVendorId()))
                .thenThrow(new ApiException(new ApiErrorResponseDto()));


        Assertions.assertThrows(ApiException.class, () -> { touristPassService.createTouristPass(touristPassInputDto); }, "API Error");

        Mockito.verify(customerService, Mockito.times(0)).findById(touristPassInputDto.getCustomerId());
        Mockito.verify(vendorService, Mockito.times(1)).findById(touristPassInputDto.getVendorId());
        Mockito.verify(touristPassRepository, Mockito.times(0)).save(any(TouristPass.class));
    }

    @Test
    public void createTouristPass_customerIdIsNull_throwsApiException() {
        touristPassInputDto.setCustomerId(null);
        Mockito.lenient().when(customerService.findById(touristPassInputDto.getCustomerId()))
                .thenThrow(new ApiException(new ApiErrorResponseDto()));

        Mockito.lenient().when(vendorService.findById(touristPassInputDto.getVendorId()))
                .thenReturn(vendorDto);

        Assertions.assertThrows(ApiException.class, () -> { touristPassService.createTouristPass(touristPassInputDto); }, "API Error");

        Mockito.verify(customerService, Mockito.times(1)).findById(touristPassInputDto.getCustomerId());
        Mockito.verify(vendorService, Mockito.times(1)).findById(touristPassInputDto.getVendorId());
        Mockito.verify(touristPassRepository, Mockito.times(0)).save(any(TouristPass.class));
    }


    @Test
    public void createTouristPass_passInLengthIsNull_throwsIllegalArgumentException() {
        touristPassInputDto.setPassLengthInHours(null);


        Assertions.assertThrows(IllegalArgumentException.class, () -> { touristPassService.createTouristPass(touristPassInputDto); }, "Pass length in hours cannot be null");

        Mockito.verify(customerService, Mockito.times(0)).findById(touristPassInputDto.getCustomerId());
        Mockito.verify(vendorService, Mockito.times(0)).findById(touristPassInputDto.getVendorId());
        Mockito.verify(touristPassRepository, Mockito.times(0)).save(any(TouristPass.class));
    }

    @Test
    public void createTouristPass_passInLengthIsLessThan1Hour_throwsIllegalArgumentException() {
        touristPassInputDto.setPassLengthInHours(0L);


        Assertions.assertThrows(IllegalArgumentException.class, () -> { touristPassService.createTouristPass(touristPassInputDto); }, "Pass in length(in Hours) cannot be less than 1 Hour");

        Mockito.verify(customerService, Mockito.times(0)).findById(touristPassInputDto.getCustomerId());
        Mockito.verify(vendorService, Mockito.times(0)).findById(touristPassInputDto.getVendorId());
        Mockito.verify(touristPassRepository, Mockito.times(0)).save(any(TouristPass.class));
    }

    @Test
    public void createTouristPass_passInLengthIsMoreThan730Hour_throwsIllegalArgumentException() {
        touristPassInputDto.setPassLengthInHours(731L);


        Assertions.assertThrows(IllegalArgumentException.class, () -> { touristPassService.createTouristPass(touristPassInputDto); }, "Pass in length(in Hours) cannot be more than 730 Hour");

        Mockito.verify(customerService, Mockito.times(0)).findById(touristPassInputDto.getCustomerId());
        Mockito.verify(vendorService, Mockito.times(0)).findById(touristPassInputDto.getVendorId());
        Mockito.verify(touristPassRepository, Mockito.times(0)).save(any(TouristPass.class));
    }


    private TouristPass getTouristData(DateTime dateTime) {
        Date expired = dateTime.plusHours(
                touristPassInputDto.getPassLengthInHours().intValue()
        ).toDate();
        return TouristPass.builder()
                .expiredAt(expired)
                .city(vendorDto.getCity())
                .issuedAt(dateTime.toDate())
                .customerId(customerDto.getId())
                .vendorId(vendorDto.getId()).build();
    }

}
