package com.ls.lps.service;

import com.ls.lps.dto.TouristPassDto;
import com.ls.lps.dto.api.CustomerDto;
import com.ls.lps.dto.api.VendorDto;
import com.ls.lps.dto.error.ApiErrorResponseDto;
import com.ls.lps.dto.input.TouristPassInputDto;
import com.ls.lps.exception.DataNotFoundException;
import com.ls.lps.exception.api.ApiException;
import com.ls.lps.mongo.entity.TouristPass;
import com.ls.lps.mongo.repository.TouristPassRepository;
import com.ls.lps.service.api.CustomerService;
import com.ls.lps.service.api.VendorService;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@Slf4j
@ExtendWith(MockitoExtension.class)
@SpringBootTest(properties = "spring.profiles.active=unit-test")
public class TouristPassServiceRenewTouristPassTests {

    @Mock
    private CustomerService customerService;

    @Mock
    private VendorService vendorService;

    @Mock
    private TouristPassRepository touristPassRepository;

    @InjectMocks
    private TouristPassService touristPassService;

    private final TouristPassInputDto touristPassInputDto = new TouristPassInputDto();

    @BeforeEach
    public void setUp() {
        touristPassInputDto.setTouristPassId("5e24676fae7d22144c65babb");
        touristPassInputDto.setPassLengthInHours(5L);
    }

    @Test
    public void renewTouristPass_touristPassInputIsFilled_methodRenewsAndReturnsTouristPassInfo() {
        DateTime dateTime = new DateTime();
        TouristPass touristPass = getTouristData(dateTime);
        Mockito.lenient().when(touristPassRepository.findById(touristPassInputDto.getTouristPassId()))
                .thenReturn(Optional.of(touristPass));

        TouristPass expectedPass = getTouristData(dateTime);
        expectedPass.setExpiredAt(
                new DateTime(touristPass.getExpiredAt())
                        .plusHours(touristPassInputDto.getPassLengthInHours().intValue())
                        .toDate()
        );

        TouristPassDto expectedPassDto = touristPassService.convertEntityToDto(expectedPass);

        Mockito.lenient().when(touristPassRepository.save(any(TouristPass.class))).thenReturn(touristPass);

        TouristPassDto touristPassDto = touristPassService.renewTouristPass(touristPassInputDto);
        Assertions.assertEquals(touristPassDto, expectedPassDto);
        Mockito.verify(touristPassRepository, Mockito.times(1)).findById(touristPassInputDto.getTouristPassId());
        Mockito.verify(touristPassRepository, Mockito.times(1)).save(any(TouristPass.class));
    }

    @Test
    public void renewTouristPass_touristPassIdIsInvalid_throwsDataNotFoundException() {
        touristPassInputDto.setTouristPassId(null);
        Mockito.lenient().when(touristPassRepository.findById(touristPassInputDto.getTouristPassId()))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(DataNotFoundException.class, () -> { touristPassService.renewTouristPass(touristPassInputDto); }, "Tourist not found for #null");

        Mockito.verify(touristPassRepository, Mockito.times(1)).findById(touristPassInputDto.getTouristPassId());
        Mockito.verify(touristPassRepository, Mockito.times(0)).save(any(TouristPass.class));
    }

    @Test
    public void renewTouristPass_passInLengthIsNull_throwsIllegalArgumentException() {
        touristPassInputDto.setPassLengthInHours(null);


        Assertions.assertThrows(IllegalArgumentException.class, () -> { touristPassService.renewTouristPass(touristPassInputDto); }, "Pass length in hours cannot be null");

        Mockito.verify(touristPassRepository, Mockito.times(0)).findById(touristPassInputDto.getTouristPassId());
        Mockito.verify(touristPassRepository, Mockito.times(0)).save(any(TouristPass.class));
    }

    @Test
    public void renewTouristPass_passInLengthIsLessThan1Hour_throwsIllegalArgumentException() {
        touristPassInputDto.setPassLengthInHours(0L);


        Assertions.assertThrows(IllegalArgumentException.class, () -> { touristPassService.renewTouristPass(touristPassInputDto); }, "Pass in length(in Hours) cannot be less than 1 Hour");

        Mockito.verify(touristPassRepository, Mockito.times(0)).findById(touristPassInputDto.getTouristPassId());
        Mockito.verify(touristPassRepository, Mockito.times(0)).save(any(TouristPass.class));
    }

    @Test
    public void renewTouristPass_passInLengthIsMoreThan730Hour_throwsIllegalArgumentException() {
        touristPassInputDto.setPassLengthInHours(731L);


        Assertions.assertThrows(IllegalArgumentException.class, () -> { touristPassService.renewTouristPass(touristPassInputDto); }, "Pass in length(in Hours) cannot be more than 730 Hour");

        Mockito.verify(touristPassRepository, Mockito.times(0)).findById(touristPassInputDto.getTouristPassId());
        Mockito.verify(touristPassRepository, Mockito.times(0)).save(any(TouristPass.class));
    }


    private TouristPass getTouristData(DateTime dateTime) {
        Date expired = dateTime.plusHours(5).toDate();
        return TouristPass.builder()
                .id(touristPassInputDto.getTouristPassId())
                .expiredAt(expired)
                .city("London")
                .issuedAt(dateTime.toDate())
                .customerId("5e24676fae7d22144c65babb")
                .vendorId("5e246ab08362930f9a15ec2c").build();
    }

}
