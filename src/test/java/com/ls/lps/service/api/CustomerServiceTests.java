package com.ls.lps.service.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ls.lps.dto.api.CustomerDto;
import com.ls.lps.dto.error.ApiErrorResponseDto;
import com.ls.lps.dto.error.ErrorResponseDetailsDto;
import com.ls.lps.exception.api.ApiException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(properties = "spring.profiles.active=unit-test")
public class CustomerServiceTests {
    @Value("${app.leisure.management.api.url}")
    private String customerUrl;
    @Autowired
    private RestTemplate leisureManagementRestTemplate;
    @Autowired
    private CustomerService customerService;

    private MockRestServiceServer mockServer = null;

    private final CustomerDto customerDto = new CustomerDto();

    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void setUp() {
        customerDto.setId("5e24676fae7d22144c65babb");
        customerDto.setEmail("vanderlinde@googlemail.com");
        customerDto.setFirstName("Dutch");
        customerDto.setLastName("Vanderlinde");
        customerDto.setCity("London");

        mockServer = MockRestServiceServer.createServer(leisureManagementRestTemplate);
    }

    @Test
    public void findById_validCustomerId_methodCallsAndReturns() throws JsonProcessingException {
        final String id = "5e24676fae7d22144c65babb";

        final String  json = objectMapper.writeValueAsString(customerDto);

        mockServer.expect(MockRestRequestMatchers.requestTo(customerUrl + "/customers/" + id))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(json));

        CustomerDto customerDto = customerService.findById(id);

        mockServer.verify();
    }

    @Test
    public void findById_invalidCustomerId_throwsApiException() throws JsonProcessingException {
        final String id = null;

        ApiErrorResponseDto errorResponseDto = new ApiErrorResponseDto();
        errorResponseDto.setStatus(HttpStatus.NOT_FOUND);

        errorResponseDto.setType("DATA_NOT_AVAILABLE");
        errorResponseDto.setErrors(Arrays.asList(new ErrorResponseDetailsDto("Customer not found for #" + id)));

        mockServer.expect(MockRestRequestMatchers.requestTo(customerUrl + "/customers/" + id))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.NOT_FOUND)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(objectMapper.writeValueAsString(errorResponseDto)));

        Assertions.assertThrows(ApiException.class, () -> { customerService.findById(id); }, "API Error");

        mockServer.verify();
    }
}
